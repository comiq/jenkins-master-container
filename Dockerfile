FROM jenkins/jenkins:lts

ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt

COPY create-admin-user.groovy /usr/share/jenkins/ref/init.groovy.d/create-admin-user.groovy
COPY executors.groovy /usr/share/jenkins/ref/init.groovy.d/executors.groovy
COPY sshconfig.groovy /usr/share/jenkins/ref/init.groovy.d/sshconfig.groovy
COPY enable-agent-protocols.groovy /usr/share/jenkins/ref/init.groovy.d/enable-agent-protocols.groovy

USER root
RUN cat /etc/passwd | awk '{ if ( NR == 1 ) print "root:x:0:0:root:/var/jenkins_home:/bin/bash" ; else print $0 }' > /etc/passwd2 && \
  cat /etc/passwd2 > /etc/passwd && \
  rm /etc/passwd2
WORKDIR /var/jenkins_home
