# Getting Started

Jenkins container uses host machines rsa id's to access team foundation server git repositories. These keys are not baked or stored inside container. Docker secrets are used to pass keys safely to container.

1.	You need to have ssh keys created and installed into tfs. Process of doing this is out of scope of this document. User must have access to repositories jenkins is using.

2. You must have working docker environment and it must be in swarm mode. Setting up docker and putting it into swarm mode is out of scope of this document.

3.	Add your private rsa-id as docker secret
```
$> cat ~/.ssh/id_rsa | docker secret create id_rsa -

```
4.	Deploy jenkins stack
```
$> docker stack deploy -c docker-compose.yml jenkins

```

5.	Access your jenkins instance with browser in http://localhost:8080


