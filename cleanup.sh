#!/bin/bash

docker stack rm jenkins

sleep 15

docker container prune -f
docker image prune -f
docker volume prune -f