command = ["sh", "-c", "mkdir /var/jenkins_home/.ssh"]
Runtime.getRuntime().exec((String[]) command.toArray())

def dockerSecretsFolder = new File('/run/secrets')
if ( dockerSecretsFolder.exists() ) {
  command = ["sh", "-c", "ln -s /run/secrets/id_rsa /var/jenkins_home/.ssh/id_rsa"]
  Runtime.getRuntime().exec((String[]) command.toArray())
}

def kubernetesSecretsFolder = new File('/var/run/secrets')
if ( kubernetesSecretsFolder.exists() ) {
  command = ["sh", "-c", "ln -s /var/run/secrets/id_rsa /var/jenkins_home/.ssh/id_rsa"]
  Runtime.getRuntime().exec((String[]) command.toArray())
}

command = ["sh", "-c", "ssh-keyscan -t rsa gitlab.com >> /var/jenkins_home/.ssh/known_hosts"]
Runtime.getRuntime().exec((String[]) command.toArray())
